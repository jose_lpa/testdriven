# Microservices with Docker, Flask and React

## Run project in Development.

Build the project and run it in local:

```
$ docker-compose -f docker-compose-dev.yml up -d --build
```

Project is available in http://localhost

## Test code.

Test the backend code:

```
$ docker-compose -f docker-compose-dev.yml exec users python manage.py test
```

Test the frontend code:

```
$ docker-compose -f docker-compose-dev.yml exec client npm test -verbose
```

## Run project in AWS.

### Simple deployments using Docker Machine.

Create new Docker host with [Docker Machine](https://docs.docker.com/machine/):

```
$ docker-machine create --driver amazonec2 testdriven-prod
```

Once done, set it as the active host and point the Docker client at it:

```
$ docker-machine env testdriven-prod
$ eval $(docker-machine env testdriven-prod)
```

Run the following command to view the currently running machines:

```
$ docker-machine ls
```

Grab the IP associated with the machine:

```
$ docker-machine ip testdriven-prod
```

Build the project and run in remote Docker host:

```
$ docker-compose -f docker-compose-prod.yml up -d --build
```

Stop the remote host:

```
$ docker-machine stop testdriven-prod
```

Point Docker back to localhost:

```
$ eval $(docker-machine env -u)
```

You can run the machine anytime again once you have created it:

```
docker-machine start tesdriven-prod
```

### Container orchestration with Amazon ECS (Elastic Container Service)

Using AWS console dashboard, navigate to **ECS** and create new repositories for
the different services.

Then, log it into AWS for ECR (Elastic Container Registry):

```
$(aws ecr get-login --region us-east-1 --no-include-email)
```

Once that is done, build, tag and push the services images:

- For ``users`` service:

```
docker build git@bitbucket.org:jose_lpa/testdriven.git#master:services/users -t test-driven-users:`git log --pretty=format:'%H' -n 1` -f Dockerfile-prod
docker tag test-driven-users:`git log --pretty=format:'%H' -n 1` 431281487009.dkr.ecr.us-east-1.amazonaws.com/test-driven-users:production
docker push 431281487009.dkr.ecr.us-east-1.amazonaws.com/test-driven-users:production
```

- For ``users_db`` service:

```
docker build git@bitbucket.org:jose_lpa/testdriven.git#master:services/users/project/db -t test-driven-users_db:`git log --pretty=format:'%H' -n 1` -f Dockerfile
docker tag test-driven-users_db:`git log --pretty=format:'%H' -n 1` 431281487009.dkr.ecr.us-east-1.amazonaws.com/test-driven-users_db:production
docker push 431281487009.dkr.ecr.us-east-1.amazonaws.com/test-driven-users_db:production
```

- For ``client`` service:

```
 docker build git@bitbucket.org:jose_lpa/testdriven.git#master:services/client -t test-driven-client:`git log --pretty=format:'%H' -n 1` -f Dockerfile-prod --build-arg REACT_APP_USERS_SERVICE_URL=http://testdriven-staging-alb-1648795403.us-east-1.elb.amazonaws.com
 docker tag test-driven-client:`git log --pretty=format:'%H' -n 1` 431281487009.dkr.ecr.us-east-1.amazonaws.com/test-driven-client:production
 docker push 431281487009.dkr.ecr.us-east-1.amazonaws.com/test-driven-client:production
```