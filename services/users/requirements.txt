Flask==1.0.2
Flask-SQLAlchemy==2.3.2
SQLAlchemy==1.2.17
flask-migrate==2.3.1
flask-restful==0.3.7
psycopg2-binary==2.7.7
flask-cors==3.0.7
gunicorn==19.9.0
pyjwt==1.7.1

# Testing toolbelt.
flask-testing==0.7.1
coverage==4.5.2
flake8==3.6.0
flask-debugtoolbar==0.10.1
