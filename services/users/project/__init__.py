import os

from flask import Flask

from flask_cors import CORS
from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


# Instantiate the DB.
db = SQLAlchemy()
toolbar = DebugToolbarExtension()
cors = CORS()
migrate = Migrate()


def create_app(script_info=None):
    # Instantiate the app.
    app = Flask(__name__)

    # Set configuration.
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # Set up extensions.
    db.init_app(app)
    toolbar.init_app(app)
    cors.init_app(app)
    migrate.init_app(app, db)

    # Register blueprints.
    from project.api.auth import auth_blueprint
    from project.api.users import users_blueprint
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(users_blueprint)

    # Shell context for Flask CLI.
    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app
